FROM node:14.17.3

WORKDIR /node/project

COPY . .

RUN npm i

CMD ["npm", "start"]